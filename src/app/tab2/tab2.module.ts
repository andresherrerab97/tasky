import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab2Page } from './tab2.page';
import { Tab2PageRoutingModule } from './tab2-routing.module';
import { ListProjectsModule } from '../componentes/projects/list-projects/list-projects.module';
import { AddProjectsModule } from '../componentes/projects/add-projects/add-projects.module';
import { AddTaskModule } from '../componentes/task/add-task/add-task.module';
import { ListTaskModule } from '../componentes/task/list-task/list-task.module';
import { ChartTaskModule } from '../componentes/task/chart-task/chart-task.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    Tab2PageRoutingModule,
    ListProjectsModule,
    AddProjectsModule,
    ListTaskModule,
    AddTaskModule,
    ChartTaskModule,
  ],
  declarations: [Tab2Page]
})
export class Tab2PageModule { }
