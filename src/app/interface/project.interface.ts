import { Task } from './task.interface'

export interface Project {
    uid?: string;
    description_project?: string;
    id_user_project?: string;
    objectives_project?: string;
    titulo_project?: string;
}