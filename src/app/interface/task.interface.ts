export interface Task {
    uid?: any;
    sum_pert?: any;
    num_fast_delivery: any;
    num_late_delivery: any;
    num_optimal_delivery: any;
    description_task: string;
    num_task?: any;
    predecesor_task?: any;
    status_task?: any;
}