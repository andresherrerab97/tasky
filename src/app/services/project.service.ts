import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Project } from '../interface/project.interface';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private afs: AngularFirestore) { }

  listProjects() {
    return this.afs.collection<Project>('projects').snapshotChanges();
  }

  addProject(project: Project) {
    return this.afs.collection('projects').add(project);
  }
}
