import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Task } from '../interface/task.interface';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private afs: AngularFirestore) { }

  listTask(uidProject: string) {
    return this.afs.collection<Task>(`projects/${uidProject}/task`).snapshotChanges();
  }
  addTask(task: Task, uidProject: string) {
    return this.afs.collection(`projects/${uidProject}/task`).add(task);
  }
  updateStatusTask(task: Task, uidProject: string) {
    return this.afs
      .collection(`projects/${uidProject}/task`)
      .doc(task.uid)
      .update(task);
  }
}
