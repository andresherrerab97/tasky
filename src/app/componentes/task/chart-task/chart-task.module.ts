import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ChartTaskComponent } from './chart-task.component';
import { NgxPrintModule } from 'ngx-print';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, FormsModule, NgxPrintModule],
    declarations: [ChartTaskComponent],
    exports: [ChartTaskComponent]
})
export class ChartTaskModule { }
