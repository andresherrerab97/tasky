import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Network, DataSet, Node, Edge, IdType } from 'vis';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { Observable } from 'rxjs';
import { User } from 'src/app/interface/user.interface';
import { Project } from 'src/app/interface/project.interface';
import { ProjectService } from 'src/app/services/project.service';
import { ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/interface/task.interface';
@Component({
  selector: 'app-chart-task',
  templateUrl: './chart-task.component.html',
  styleUrls: ['./chart-task.component.scss'],
})
export class ChartTaskComponent implements OnInit {
  user$: Observable<User> = this.authSvc.afAuth.user;
  data: Project[];
  dataTask: Task[];
  //@ViewChild('content') content: ElementRef;
  uidProyecto:string;

  constructor(private projectSvc: ProjectService,
    public modalController: ModalController,
    private authSvc: AuthService,
    private taskSvc: TaskService) { }

  ngOnInit() {
    this.listProject();
    // network.on("afterDrawing", function (ctx) {
    //   var dataURL = ctx.canvas.toDataURL();
    //   document.getElementById('canvasImg').setAttribute("src", dataURL);
    // });
  }

  listProject() {
    this.user$.subscribe(user => {
      this.projectSvc.listProjects().subscribe((projectSnapshot) => {
        this.data = [];
        projectSnapshot.forEach((projectData: any) => {
          if (projectData.payload.doc.data().id_user_project === user.uid) {
            this.data.push({
              uid: projectData.payload.doc.id,
              titulo_project: projectData.payload.doc.data().titulo_project,
              objectives_project: projectData.payload.doc.data().objectives_project,
              description_project: projectData.payload.doc.data().description_project,
              id_user_project: projectData.payload.doc.data().id_user_project
            });
          }
        })
      });
    });
  }

  listTask(event: any) {
    let uidPro = event.target.value;
    this.taskSvc.listTask(uidPro).subscribe(async (taskSnapshot) => {
      this.dataTask = [];
      await taskSnapshot.forEach((taskData: any) => {
        this.dataTask.push({
          uid: taskData.payload.doc.id,
          sum_pert: taskData.payload.doc.data().sum_pert,
          num_fast_delivery: taskData.payload.doc.data().num_fast_delivery,
          num_late_delivery: taskData.payload.doc.data().num_late_delivery,
          num_optimal_delivery: taskData.payload.doc.data().num_optimal_delivery,
          description_task: taskData.payload.doc.data().description_task,
          num_task: taskData.payload.doc.data().num_task,
          predecesor_task: taskData.payload.doc.data().predecesor_task,
          status_task: parseInt(taskData.payload.doc.data().status_task)
        });
        
      });

      await this.orderData();
    });
  }
  orderData() {
    this.dataTask.sort((a, b) => a.num_task - b.num_task);
    this.chartRender();
  }

  chartRender(){
    let pushNodes = [];
    let predecesores = [];
    this.dataTask.forEach((task:Task) => {
      pushNodes.push({ id: task.num_task , label: task.description_task });
      predecesores.push({ from: task.num_task , to: task.predecesor_task })
    });

    var nodes = new DataSet(pushNodes);
    // create an array with edges
    var edges = new DataSet(predecesores);
    // create a network
    var container = document.getElementById('mynetwork');
    var data = {
      nodes: nodes,
      edges: edges
    };
    var options = {
      edges: {
        smooth: {
          type: "cubicBezier",
          forceDirection: "vertical",
          roundness: 0.4
        }
      },
      layout: {
        hierarchical: {
          direction: "UD"
        }
      },
      physics: false
    };
    var network = new Network(container, data, options);
  }

  // imprimirPdf() {
  //   let doc = new jsPDF();
  //   let specialElementHandlers = {
  //     '#editor': function (element, renderer) {
  //       return true;
  //     }
  //   };
  //   let content = document.getElementById("content");

  //   doc.fromHTML(content.innerHTML, 15, 15, {
  //     'width': 190,
  //     'elementHandlers': specialElementHandlers
  //   });

  //   doc.save('test.pdf');
  // }

}
