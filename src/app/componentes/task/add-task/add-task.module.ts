import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { AddTaskComponent } from './add-task.component';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, FormsModule],
    declarations: [AddTaskComponent],
    exports: [AddTaskComponent]
})
export class AddTaskModule { }
