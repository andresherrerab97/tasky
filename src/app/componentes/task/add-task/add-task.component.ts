import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Project } from 'src/app/interface/project.interface';
import { Task } from 'src/app/interface/task.interface';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss'],
})
export class AddTaskComponent implements OnInit {
  @Input() project: Project;
  data: Task[];
  task: Task = {
    sum_pert: 0,
    num_fast_delivery: 0,
    num_late_delivery: 0,
    num_optimal_delivery: 0,
    description_task: '',
    status_task: 1,
    num_task: 1,
    predecesor_task: 0
  };

  constructor(
    private taskSvc: TaskService,
    public modalController: ModalController,
    private toastController: ToastController,
  ) { }

  ngOnInit() {

    this.listTask();
  }

  listTask() {
    this.taskSvc.listTask(this.project.uid).subscribe((taskSnapshot) => {
      this.data = [];
      taskSnapshot.forEach((taskData: any) => {
        this.data.push({
          uid: taskData.payload.doc.id,
          sum_pert: taskData.payload.doc.data().sum_pert,
          num_fast_delivery: taskData.payload.doc.data().num_fast_delivery,
          num_late_delivery: taskData.payload.doc.data().num_late_delivery,
          num_optimal_delivery: taskData.payload.doc.data().num_optimal_delivery,
          description_task: taskData.payload.doc.data().description_task,
          num_task: taskData.payload.doc.data().num_task,
          predecesor_task: taskData.payload.doc.data().predecesor_task,
          status_task: taskData.payload.doc.data().status_task
        });
      })
    });
  }

  async logForm(form) {
    let validFormInputs = this.validForm(this.task);
    if (validFormInputs.valid === true) {
      this.task.sum_pert = this.calculoPert(this.task);
      if (this.data === undefined) {
        this.task.num_task = 1
      } else {
        this.task.num_task = this.data.length + 1;
      }
      if (this.task.predecesor_task === undefined) {
        this.task.predecesor_task = 0;
      }
      this.taskSvc.addTask(this.task, this.project.uid)
        .then(async () => {
          this.task = {
            sum_pert: 0,
            num_fast_delivery: 0,
            num_late_delivery: 0,
            num_optimal_delivery: 0,
            description_task: '',
            status_task: 1,
            num_task: 1,
            predecesor_task: 0
          };
          const toast = await this.toastController.create({
            message: "Se Guardo Correctamente la Tarea",
            duration: 2000
          });
          toast.present();
        })
        .catch(error => {
          console.log(error)
        });
    }
    else {
      const toast = await this.toastController.create({
        message: validFormInputs.mensaje,
        duration: 2000
      });
      toast.present();
    }
  }

  calculoPert(task: Task) {
    let tiempoOptimoMult = 4 * task.num_optimal_delivery;
    let sum = task.num_fast_delivery + tiempoOptimoMult + task.num_late_delivery;
    let result = sum / 6;
    return result;
  }

  validForm(task: Task) {
    let res = {
      valid: true,
      mensaje: ""
    }
    if (task.description_task.trim().length === 0) {
      res.valid = false;
      res.mensaje = "Descripción de la Tarea Requerido";
      return res;
    }
    else if (this.data === undefined || this.data.length === 0) {
      if (task.predecesor_task === 0) {
        res.valid = false;
        res.mensaje = "Predecesor de la Tarea Requerido";
        return res;
      }
    } else if (task.num_fast_delivery === 0) {
      res.valid = false;
      res.mensaje = "Entrega Rapida es Requerida";
      return res;
    } else if (task.num_optimal_delivery === 0) {
      res.valid = false;
      res.mensaje = "Entrega Optima es Requerida";
      return res;
    } else if (task.num_late_delivery === 0) {
      res.valid = false;
      res.mensaje = "Entrega Tardía es Requerida";
      return res;
    }

    return res;

  }
  close() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
