import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ListTaskComponent } from './list-task.component';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, FormsModule],
    declarations: [ListTaskComponent],
    exports: [ListTaskComponent]
})
export class ListTaskModule { }
