import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Project } from 'src/app/interface/project.interface';
import { Task } from 'src/app/interface/task.interface';
import { TaskService } from 'src/app/services/task.service';
import { AddTaskComponent } from '../add-task/add-task.component';
import { DetailTaskComponent } from '../detail-task/detail-task.component';

@Component({
  selector: 'app-list-task',
  templateUrl: './list-task.component.html',
  styleUrls: ['./list-task.component.scss'],
})
export class ListTaskComponent implements OnInit {
  @Input() project: Project;
  data: Task[];
  constructor(private taskSvc: TaskService, public modalController: ModalController) { }

  ngOnInit() {
    this.listTask();
  }

  listTask() {
    this.taskSvc.listTask(this.project.uid).subscribe(async (taskSnapshot) => {
      this.data = [];
      await taskSnapshot.forEach((taskData: any) => {
        this.data.push({
          uid: taskData.payload.doc.id,
          sum_pert: taskData.payload.doc.data().sum_pert,
          num_fast_delivery: taskData.payload.doc.data().num_fast_delivery,
          num_late_delivery: taskData.payload.doc.data().num_late_delivery,
          num_optimal_delivery: taskData.payload.doc.data().num_optimal_delivery,
          description_task: taskData.payload.doc.data().description_task,
          num_task: taskData.payload.doc.data().num_task,
          predecesor_task: taskData.payload.doc.data().predecesor_task,
          status_task: parseInt(taskData.payload.doc.data().status_task)
        });
        
      });

      await this.orderData();
    });
  }
  orderData() {
    this.data.sort((a, b) => a.num_task - b.num_task);
    console.log(this.data);
  }
  async presentModalAdd() {
    const modal = await this.modalController.create({
      component: AddTaskComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        'project': this.project
      }
    });
    return await modal.present();
  }

  async presentModalDetail(task: Task) {
    const modal = await this.modalController.create({
      component: DetailTaskComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        'task': task,
        'project': this.project
      }
    });
    return await modal.present();
  }

  close() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
