import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Project } from 'src/app/interface/project.interface';
import { Task } from 'src/app/interface/task.interface';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-detail-task',
  templateUrl: './detail-task.component.html',
  styleUrls: ['./detail-task.component.scss'],
})
export class DetailTaskComponent implements OnInit {
  @Input() task: Task;
  @Input() project: Project;
  constructor(
    public modalController: ModalController,
    private taskSvc: TaskService,
    private toastController: ToastController
  ) { }

  ngOnInit() { }

  close() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }


  updateEstado(event: any) {
    debugger;
    this.task.status_task = event.target.value;
    this.taskSvc.updateStatusTask(this.task, this.project.uid)
      .then(async () => {
        const toast = await this.toastController.create({
          message: "Se Actualizo el Estado Correctamente",
          duration: 2000
        });
        toast.present();
      })
      .catch(async (error) => {
        const toast = await this.toastController.create({
          message: "Error al actualizar el estado",
          duration: 2000
        });
        toast.present();
      })
  }

}
