import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { DetailTaskComponent } from './detail-task.component';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, FormsModule],
    declarations: [DetailTaskComponent],
    exports: [DetailTaskComponent]
})
export class DetailTaskModule { }
