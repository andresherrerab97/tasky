import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from 'src/app/interface/project.interface';
import { Task } from 'src/app/interface/task.interface';
import { ProjectService } from 'src/app/services/project.service';
import { TaskService } from 'src/app/services/task.service';
import { User } from '../../interface/user.interface';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-explore-container',
  templateUrl: './explore-container.component.html',
  styleUrls: ['./explore-container.component.scss'],
})
export class ExploreContainerComponent implements OnInit {
  @Input() name: string;
  user$: Observable<User> = this.authSvc.afAuth.user;
  data: Project[];
  numProject:any;
  nivelTask:any;
  dataTask:Task[] = [];
  cont:any;


  constructor(private authSvc: AuthService,private projectSvc: ProjectService,private taskSvc: TaskService) { }

  ngOnInit() {
    this.user$.subscribe(user => {
      console.log(user);
    });
    this.listProject();
  }

  listProject() {
    this.user$.subscribe(user => {
      this.projectSvc.listProjects().subscribe(async (projectSnapshot) => {
        this.data = [];
        await projectSnapshot.forEach((projectData: any) => {
          if (projectData.payload.doc.data().id_user_project === user.uid) {
            this.data.push({
              uid: projectData.payload.doc.id,
              titulo_project: projectData.payload.doc.data().titulo_project,
              objectives_project: projectData.payload.doc.data().objectives_project,
              description_project: projectData.payload.doc.data().description_project,
              id_user_project: projectData.payload.doc.data().id_user_project
            });
          }
        });

        await this.ListTask();
      });
    });

  }
  
  ListTask(){
    this.numProject = this.data.length;
    this.data.forEach((project:Project) => {
      this.taskSvc.listTask(project.uid).subscribe( (taskSnapshot) => {
        for (let i = 0; i < taskSnapshot.length; i++) {
          this.dataTask.push({
            uid: taskSnapshot[i].payload.doc.id,
            sum_pert: taskSnapshot[i].payload.doc.data().sum_pert,
            num_fast_delivery: taskSnapshot[i].payload.doc.data().num_fast_delivery,
            num_late_delivery: taskSnapshot[i].payload.doc.data().num_late_delivery,
            num_optimal_delivery: taskSnapshot[i].payload.doc.data().num_optimal_delivery,
            description_task: taskSnapshot[i].payload.doc.data().description_task,
            num_task: taskSnapshot[i].payload.doc.data().num_task,
            predecesor_task: taskSnapshot[i].payload.doc.data().predecesor_task,
            status_task: parseInt(taskSnapshot[i].payload.doc.data().status_task)
          });
        }
         this.levelTask();
      });
    });
  }

  levelTask(){
    let taskFinalizadas =  this.dataTask.filter(x => x.status_task === 3).length;
    this.nivelTask = (taskFinalizadas / this.dataTask.length) * 100;
  }

}
