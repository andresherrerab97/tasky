import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Project } from 'src/app/interface/project.interface';
import { ProjectService } from 'src/app/services/project.service';
import { AddProjectsComponent } from '../add-projects/add-projects.component';
import { ListTaskComponent } from '../../task/list-task/list-task.component';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/interface/user.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list-projects',
  templateUrl: './list-projects.component.html',
  styleUrls: ['./list-projects.component.scss'],
})
export class ListProjectsComponent implements OnInit {
  user$: Observable<User> = this.authSvc.afAuth.user;
  data: Project[];
  constructor(
    private projectSvc: ProjectService,
    public modalController: ModalController,
    private authSvc: AuthService
  ) { }

  ngOnInit() {
    this.listProject();
  }

  listProject() {
    this.user$.subscribe(user => {
      this.projectSvc.listProjects().subscribe((projectSnapshot) => {
        this.data = [];
        projectSnapshot.forEach((projectData: any) => {
          if (projectData.payload.doc.data().id_user_project === user.uid) {
            this.data.push({
              uid: projectData.payload.doc.id,
              titulo_project: projectData.payload.doc.data().titulo_project,
              objectives_project: projectData.payload.doc.data().objectives_project,
              description_project: projectData.payload.doc.data().description_project,
              id_user_project: projectData.payload.doc.data().id_user_project
            });
          }
        })
      });
    });
  }

  async presentModalListTask(project: Project) {
    const modal = await this.modalController.create({
      component: ListTaskComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        'project': project
      }
    });
    return await modal.present();
  }


  async presentModal() {
    const modal = await this.modalController.create({
      component: AddProjectsComponent,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
