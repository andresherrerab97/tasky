import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ListProjectsComponent } from './list-projects.component';


@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, FormsModule],
    declarations: [ListProjectsComponent],
    exports: [ListProjectsComponent]
})
export class ListProjectsModule { }
