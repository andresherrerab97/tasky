import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Project } from 'src/app/interface/project.interface';
import { User } from 'src/app/interface/user.interface';
import { AuthService } from 'src/app/services/auth.service';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-add-projects',
  templateUrl: './add-projects.component.html',
  styleUrls: ['./add-projects.component.scss'],
})
export class AddProjectsComponent implements OnInit {

  project: Project = {
    titulo_project: '',
    description_project: '',
    objectives_project: '',
    id_user_project: ''
  };
  user$: Observable<User> = this.authSvc.afAuth.user;

  constructor(
    private authSvc: AuthService,
    private projectSvc: ProjectService,
    private toastController: ToastController,
    private modalController: ModalController
  ) { }

  ngOnInit() {
  }


  async logForm(form) {
    let validFormInputs = this.validForm(this.project);
    if (validFormInputs.valid === true) {
      this.user$.subscribe(user => {
        this.project.id_user_project = user.uid;
        this.projectSvc.addProject(this.project)
          .then(async () => {
            this.project = {
              titulo_project: '',
              description_project: '',
              objectives_project: '',
              id_user_project: ''
            };
            const toast = await this.toastController.create({
              message: "Se Guardo Correctamente el Proyecto",
              duration: 2000
            });
            toast.present();
          })
          .catch(error => {
            console.log(error)
          })
      });
    } else {
      const toast = await this.toastController.create({
        message: validFormInputs.mensaje,
        duration: 2000
      });
      toast.present();
    }
  }

  validForm(project: Project) {
    let valid = true;
    let mensaje = "";
    if (project.titulo_project.trim().length === 0) {
      valid = false;
      mensaje = "Titulo Proyecto Requerido";
    }
    else if (project.description_project.trim().length === 0) {
      valid = false;
      mensaje = "Descripción Proyecto Requerido";
    } else if (project.objectives_project.trim().length === 0) {
      valid = false;
      mensaje = "Objetivos Proyecto Requerido";
    }
    let res = {
      valid,
      mensaje
    }
    return res;
  }


  close() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
