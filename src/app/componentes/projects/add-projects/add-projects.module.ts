import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { AddProjectsComponent } from './add-projects.component';


@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, FormsModule],
    declarations: [AddProjectsComponent],
    exports: [AddProjectsComponent]
})
export class AddProjectsModule { }
