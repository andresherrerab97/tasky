import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private authSvc: AuthService, private router: Router, private toastController: ToastController) { }

  ngOnInit() { }


  async onLogin(email, password) {
    try {
      const user = await this.authSvc.login(email.value, password.value);
      if (user) {
        this.redirectUser();
      } else {
        const toast = await this.toastController.create({
          message: "Email o Contraseña Incorrectas",
          duration: 2000
        });
        toast.present();
      }
    } catch (error) {
      const toast = await this.toastController.create({
        message: "Email o Contraseña Incorrectas",
        duration: 2000
      });
      toast.present();
      console.log('Error-->', error)
    }
  }

  async onLoginGoogle() {
    try {
      const user = await this.authSvc.loginGoogle();
      if (user) {
        this.redirectUser();
      }
    } catch (error) {
      console.log('Error-->', error)
    }
  }

  redirectUser() {
    this.router.navigate(['']);
  }

}
