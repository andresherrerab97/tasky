import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { AuthService } from '../../services/auth.service';
import validator from 'validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {


  constructor(private authSvc: AuthService, private router: Router, private toastController: ToastController) { }

  ngOnInit() { }

  async onRegister(email, password, passwordconfirm, nombre) {

    let validFormInputs = this.validForm(email.value, password.value, passwordconfirm.value, nombre.value);
    if (validFormInputs.valid === true) {
      try {
        const user = await this.authSvc.register(email.value, password.value, nombre.value);
        if (user) {
          this.redirectUser();
        }
      } catch (error) {
        console.log('Error-->', error);
      }
    } else {
      const toast = await this.toastController.create({
        message: validFormInputs.mensaje,
        duration: 2000
      });
      toast.present();
    }

  }

  validForm(email: string, password: string, passwordconfirm: string, nombre: string) {
    let valid = true;
    let mensaje = "";
    if (nombre.length === 0) {
      valid = false;
      mensaje = "Nombre Requerido";
    } else if (!validator.isEmail(email)) {
      valid = false;
      mensaje = "Email Requerido";
    }
    else if (password.trim().length === 0) {
      valid = false;
      mensaje = "Contraseña Requerida";
    } else if (password.length < 6) {
      valid = false;
      mensaje = "Contraseña debe contener 6 caracteres";
    } else if (password.trim() !== passwordconfirm.trim()) {
      valid = false;
      mensaje = "Las contraseñas deben ser iguales";
    }
    let res = {
      valid,
      mensaje
    }
    return res;
  }

  redirectUser() {
    this.router.navigate(['']);
  }

}
